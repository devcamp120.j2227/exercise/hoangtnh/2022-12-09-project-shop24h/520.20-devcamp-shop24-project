import { Col, Container, Row } from "reactstrap";
import img1 from "../../../assets/about/person_3.jpg";
import img2 from "../../../assets/about/person_4.jpg";
const AboutUs = () => {
    return (
        <div className="div-lastestProduct">
            <Container style={{paddingBottom:"100px"}}>
                    <Col>
                        <Row className="d-flex justify-content-end mb-5" style={{paddingTop:"50px"}} >
                            <Col sm={3} className="d-flex justify-content-end" >
                                <div><hr style={{width:"50px",border:"1px solid orange"}}/></div>
                            </Col>
                            <Col sm={2} style={{marginRight:"50px"}}>
                                <h4>About us</h4>
                            </Col>
                        </Row>
                    </Col>
                    <Row>
                        <Col style={{ padding: "10px",boxShadow: "5px 10px 18px #888888", margin: "60px"}}>
                            <Row>
                                <Col sm={3}>
                                    <img src={img1} style={{borderRadius:"50px", width:"100px"}}/>
                                </Col>
                                <Col sm={9} className="d-flex align-items-center">
                                    <Row>
                                        <h6>Magna</h6>
                                        <p style={{color:"#d1c286"}}>Cons ectetur adipiscing</p>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudLorem ipsum
                                </p>
                            </Row>
                        </Col>
                        <Col style={{ padding: "10px",boxShadow: "5px 10px 18px #888888",margin: "60px"}}>
                            <Row>
                                <Col sm={3}>
                                    <img src={img2} style={{borderRadius:"50px", width:"100px"}}/>
                                </Col>
                                <Col sm={9} className="d-flex align-items-center">
                                    <Row>
                                        <h6>Aliqua</h6>
                                        <p style={{color:"#d1c286"}}>Cons ectetur adipiscing</p>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudLorem ipsum
                                </p>
                            </Row>
                            
                        </Col>
                    </Row>
            </Container>
        </div>
    )
}
export default AboutUs;