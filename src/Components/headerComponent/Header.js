import { Col, Row,Container } from "reactstrap";
import '@fortawesome/fontawesome-free/css/all.min.css';
const Header = () =>{
    return (
        <>
        <div style={{backgroundColor:"#d1c286"}}>
            <Container >
                <Row style={{padding:"10px", backgroundColor:"#d1c286"}}>
                    <Col sm={3} className="d-flex align-items-center justify-content-center">
                        <h3 style={{color:"white"}}>Devcamp COSY-HOME</h3>
                    </Col>
                    <Col sm={6} className="d-flex align-items-center justify-content-center" style={{padding:"0px"}}>
                        <Row sm={12} style={{margin:"0px"}}>
                            <Col sm={3} className = "header-content" >
                                <a type="button" style={{padding:"5px"}}>SHOP</a>
                            </Col>
                            <Col sm={3} className = "header-content" >
                                <a type="button"style={{padding:"5px"}} >ABOUT</a>
                            </Col>
                            <Col sm={3} className = "header-content" >
                                <a type="button" style={{padding:"5px"}} >SERVICES</a>
                            </Col>
                            <Col sm={3} className = "header-content" >
                                <a type="button"style={{padding:"5px"}} >CONTACT</a>
                            </Col>
                        </Row>
                    </Col>
                    <Col sm={3} className="d-flex align-items-center justify-content-end">
                        <Col sm={4}>
                            <p style={{margin:"0px"}}><i type="button" className="fa-solid fa-magnifying-glass"></i> Search</p>
                        </Col>
                        <Col sm={4}>
                            <p style={{margin:"0px"}}><i type="button" className="fa-solid fa-cart-shopping"></i> Cart</p>
                        </Col>
                    </Col>
                </Row>
            </Container>
        </div>
        </>
    )
}
export default Header;