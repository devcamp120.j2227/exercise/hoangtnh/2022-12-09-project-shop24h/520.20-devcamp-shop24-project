import { Button, Col, Container, Row } from "reactstrap";
import { useState } from "react";
import imgCategory from "../../../assets/slideImg/slide3.webp"
import '@fortawesome/fontawesome-free/css/all.min.css';
import data from "../../../assets/data/ProductData.json";

const fetchApi = async(url, body) =>{
    const response = await fetch(url,body);
    const data = await response.json();
    
    return data;
}

const Categories = () =>{
    const [Product, showProduct] = useState([]);
    const [btnClick, setStatus] = useState(false);
    const [btnMore, setStatusDefault] = useState(false);

    const livingRoomBtn = () =>{
        //Code sử dụng database từ mongoose được tạo từ node.js bài 520.10 
    // fetchApi("http://localhost:8000/api/products/description?DescriptionProduct=LivingRoom")
    //     .then((data) =>{
    //         showProduct(data.Product)
    //         setStatus(true);
    //     })
    //     .catch((error)=>{
    //         console.log(error.message)
    //     })
    
    //code sử dụng data từ data json được tạo
    let result = data.Products.filter( ({Description}) => Description ==="LivingRoom");
    console.log(result);
    showProduct(result);
    setStatus(true);
    }
    const bedRoomBtn = ()=>{
        //Code sử dụng database từ mongoose được tạo từ node.js bài 520.10 
    // fetchApi("http://localhost:8000/api/products/description?DescriptionProduct=BedRoom")
    // .then((data) =>{
    //     showProduct(data.Product)
    //     setStatus(true);
    // })
    // .catch((error)=>{
    //     console.log(error.message)
    // })

        //code sử dụng data từ data json được tạo
    let result = data.Products.filter( ({Description}) => Description ==="BedRoom");
    console.log(result);
    showProduct(result);
    setStatus(true);
    }
    const LoftRoomBtn = ()=>{
        //Code sử dụng database từ mongoose được tạo từ node.js bài 520.10 
    // fetchApi("http://localhost:8000/api/products/description?DescriptionProduct=LoftRoom")
    // .then((data) =>{
    //     showProduct(data.Product)
    //     setStatus(true);
    // })
    // .catch((error)=>{
    //     console.log(error.message)
    // })
            //code sử dụng data từ data json được tạo
        let result = data.Products.filter( ({Description}) => Description ==="LoftRoom");
        console.log(result);
        showProduct(result);
        setStatus(true);
    }
    
    const KitchenBtn = ()=>{
        //Code sử dụng database từ mongoose được tạo từ node.js bài 520.10 
        // fetchApi("http://localhost:8000/api/products/description?DescriptionProduct=Kitchen")
        // .then((data) =>{
        //     showProduct(data.Product)
        //     setStatus(true);
        // })
        // .catch((error)=>{
        //     console.log(error.message)
        // })
             //code sử dụng data từ data json được tạo
        let result = data.Products.filter( ({Description}) => Description ==="Kitchen");
        console.log(result);
        showProduct(result);
        setStatus(true);
    }
    return (
        <div className="div-categories">
            <Container>
                <Row>
                    <Col style={{marginTop:"50px"}}>
                        <Row className="justify-content-center">
                            <Col sm={2} style={{marginRight:"20px"}}>
                                <h4>Categories</h4>
                            </Col>
                            <Col sm={3}>
                                <hr style={{width:"50px",border:"1px solid orange"}}/>
                            </Col>
                        </Row>
                        <Row>
                            <Row className="d-flex align-items-center mt-5">
                                <Col sm={1} style={{display:"flex",padding:"0px", justifyContent:"flex-end"}}>
                                    <p type="button" onClick={livingRoomBtn} style={{margin:"0px",color: "#d1c286"}}>01</p>
                                </Col>
                                <Col sm={1}>
                                    <hr style={{width:"30px",border:"1px solid orange"}}/>
                                </Col>
                                <Col sm={3}>
                                    <p type="button" onClick={livingRoomBtn} style={{margin:"0px"}}><b>Living Room</b></p>
                                </Col>
                            </Row>
                        </Row>
                        <Row>
                            <Row className="d-flex align-items-center mt-5">
                                <Col sm={1} style={{display:"flex",padding:"0px", justifyContent:"flex-end"}}>
                                    <p type="button" onClick={bedRoomBtn} style={{margin:"0px",color: "#d1c286"}}>02</p>
                                </Col>
                                <Col sm={1}>
                                    <hr style={{width:"30px",border:"1px solid orange"}}/>
                                </Col>
                                <Col sm={3}>
                                    <p type="button" onClick={bedRoomBtn} style={{margin:"0px"}}><b>Bed Room</b></p>
                                </Col>
                            </Row>
                        </Row>
                        <Row>
                            <Row className="d-flex align-items-center mt-5">
                                <Col sm={1} style={{display:"flex",padding:"0px", justifyContent:"flex-end"}}>
                                    <p type="button" onClick={LoftRoomBtn} style={{margin:"0px",color: "#d1c286"}}>03</p>
                                </Col>
                                <Col sm={1}>
                                    <hr style={{width:"30px",border:"1px solid orange"}}/>
                                </Col>
                                <Col sm={3}>
                                    <p type="button" onClick={LoftRoomBtn} style={{margin:"0px"}}><b>Loft Room</b></p>
                                </Col>
                            </Row>
                        </Row>
                        <Row>
                            <Row className="d-flex align-items-center mt-5">
                                <Col sm={1} style={{display:"flex",padding:"0px", justifyContent:"flex-end"}}>
                                    <p type="button" onClick={KitchenBtn} style={{margin:"0px",color: "#d1c286"}}>04</p>
                                </Col>
                                <Col sm={1}>
                                    <hr style={{width:"30px",border:"1px solid orange"}}/>
                                </Col>
                                <Col sm={3}>
                                    <p type="button" onClick={KitchenBtn} style={{margin:"0px"}}><b>Kitchen</b></p>
                                </Col>
                            </Row>
                        </Row>
                        <Row>
                            <Button style={{width:"60%",color:"#d1c286",marginTop:"100px"}} onClick={()=>{setStatusDefault(true); setStatus(false)}}> For More Product <i className="fa-solid fa-angles-right"></i></Button>
                        </Row>
                    </Col>
                    <Col style={{marginTop:"100px"}}>
                        <Row style={{display: "flex" ,flexDirection: "row"}}>
                        {btnClick === true ? Product.map((value,index)=>{
                            return(
                                <Col sm={4} key={index}  className="d-flex justify-content-center" >
                                    <div className="text-center img-product">
                                        <img src={value.ImageUrl} style={{width:"160px", height:"160px"}}/>
                                        <div style={{marginTop:"10px"}}>
                                            <h6 style={{margin:"0px"}}>{value.Name}</h6>
                                            <p style={{textDecoration: "line-through", fontSize:"smaller", margin:"0px"}}>${value.BuyPrice}</p>
                                            <p style={{marginTop:"-5px"}}>${value.PromotionPrice}</p>
                                        </div>
                                    </div>
                                </Col>
                            )
                        }):<img src={imgCategory} style={{borderRadius:"5px", padding:"0px"}}/>}
                        </Row>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
export default Categories;