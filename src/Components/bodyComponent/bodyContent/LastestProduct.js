import { Container, Row, Col } from "reactstrap";
import imgLastest1 from "../../../assets/lasterProduct/img_bg_1.jpg"
import imgLastest2 from "../../../assets/lasterProduct/img_bg_3.jpg"
import imgLastest3 from "../../../assets/lasterProduct/product-7.jpg"
import imgLastest4 from "../../../assets/lasterProduct/product-8.jpg"
import '@fortawesome/fontawesome-free/css/all.min.css';
import { useState } from "react";
const icon = <i className="fa-solid fa-angles-right" ></i>;
const LastestProduct = () => {
    const [showImage1, setShowImage1] = useState(false);
    const [showImage2, setShowImage2] = useState(false);
    const [showImage3, setShowImage3] = useState(false);
    const [showImage4, setShowImage4] = useState(false);

    return(
        <div className="div-lastestProduct">
            <Container style={{paddingTop:"50px"}}>
                <Col>
                    <Row className="d-flex justify-content-end" >
                        <Col sm={3} className="d-flex justify-content-end" >
                            <div><hr style={{width:"50px",border:"1px solid orange"}}/></div>
                        </Col>
                        <Col sm={2} style={{marginRight:"50px"}}>
                            <h4>Lastest Product</h4>
                        </Col>
                    </Row>
                </Col>
                <Row className="text-center">
                    <Col sm={6} >
                        <Container >
                        <div className="box">
                            <img className="img-lastestProduct" src={imgLastest1} style={{ filter: showImage1 ? "brightness(70%)" : "brightness(100%)"  }} onMouseEnter={() => setShowImage1(true)}
                                onMouseLeave={() => setShowImage1(false)}/>
                            <div className="overlay">
                                <a href="#" className="icon">
                                    <i type="button" className="fa-solid fa-cart-shopping"></i>
                                </a>
                            </div>
                        </div>
                        </Container>
                        <h5 style={{marginTop:"5px", marginBottom:"0px"}}>ALATO CABINET</h5>
                        <p>$550</p>
                    </Col>
                    <Col sm={6}>
                    <Container >
                        <div className="box">
                            <img className="img-lastestProduct" src={imgLastest2} style={{ filter: showImage2 ? "brightness(70%)" : "brightness(100%)"  }} onMouseEnter={() => setShowImage2(true)}
                                onMouseLeave={() => setShowImage2(false)}/>
                            <div className="overlay">
                                <a href="#" className="icon">
                                    <i type="button" className="fa-solid fa-cart-shopping"></i>
                                </a>
                            </div>
                        </div>
                    </Container>
                        <h5 style={{marginTop:"5px", marginBottom:"0px"}}>LIGOMANCER</h5>
                        <p>$700</p>
                    </Col>
                </Row>
                <Row className="text-center">
                    <Col sm={6}>
                    <Container >
                        <div className="box">
                            <img className="img-lastestProduct" src={imgLastest3} style={{ filter: showImage3 ? "brightness(70%)" : "brightness(100%)"  }} onMouseEnter={() => setShowImage3(true)}
                                onMouseLeave={() => setShowImage3(false)}/>
                            <div className="overlay">
                                <a href="#" className="icon">
                                    <i type="button" className="fa-solid fa-cart-shopping"></i>
                                </a>
                            </div>
                        </div>
                    </Container>
                        <h5 style={{marginTop:"5px", marginBottom:"0px"}}>THE WW CHAIR</h5>
                        <p>$540</p>
                    </Col>
                    <Col sm={6}>
                    <Container >
                        <div className="box">
                            <img className="img-lastestProduct" src={imgLastest4} style={{ filter: showImage4 ? "brightness(70%)" : "brightness(100%)"  }} onMouseEnter={() => setShowImage4(true)}
                                onMouseLeave={() => setShowImage4(false)}/>
                            <div className="overlay">
                                <a href="#" className="icon">
                                    <i type="button" className="fa-solid fa-cart-shopping"></i>
                                </a>
                            </div>
                        </div>
                    </Container>
                        <h5 style={{marginTop:"5px", marginBottom:"0px"}}>HIMITSU MONEY BOX</h5>
                        <p>$80</p>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
export default LastestProduct;